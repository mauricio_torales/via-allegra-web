$(document).ready(function() {
  var $window = $(window);
  function checkWidth() {
     var windowsize = $window.width();
     if (windowsize > 440) {
        $(document).scroll(function () {
          var y = $(this).scrollTop();
          if ( y > 400 && y < 900) {
            $('#bs-example-navbar-collapse-1 .home').addClass('active');
             $('#bs-example-navbar-collapse-1 .nav-nosotros').removeClass('active');
                
          }else if ( y > 900 && y < 2000) {
               $('#bs-example-navbar-collapse-1 .nav-nosotros').addClass('active');
                $('#bs-example-navbar-collapse-1 .home').removeClass('active');
                $('#bs-example-navbar-collapse-1 .nav-locales').removeClass('active');
          }else if ( y > 2000 && y < 3000) {
               $('#bs-example-navbar-collapse-1 .nav-locales').addClass('active');
                $('#bs-example-navbar-collapse-1 .nav-nosotros').removeClass('active');
                $('#bs-example-navbar-collapse-1 .nav-eventos').removeClass('active');
          }else if ( y > 3000 && y < 4000) {
               $('#bs-example-navbar-collapse-1 .nav-eventos').addClass('active');
                $('#bs-example-navbar-collapse-1 .nav-locales').removeClass('active');
                $('#bs-example-navbar-collapse-1 .nav-galeria').removeClass('active');
          }else if ( y > 4000 && y < 4800) {
               $('#bs-example-navbar-collapse-1 .nav-galeria').addClass('active');
                $('#bs-example-navbar-collapse-1 .nav-eventos').removeClass('active');
                $('#bs-example-navbar-collapse-1 .nav-contacto').removeClass('active');
          }else if( y > 4800 && y < 7000) {
               $('#bs-example-navbar-collapse-1 .nav-contacto').addClass('active');
                $('#bs-example-navbar-collapse-1 .nav-galeria').removeClass('active');
          }
      })

}}
 // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);
});


        $(document).ready(function () {
        $('#bs-example-navbar-collapse-1 .home').click(function() {
        $(".m-p-g__controls-close").click();

      });
         $('#bs-example-navbar-collapse-1 .nav-nosotros').click(function() {
           $(".m-p-g__controls-close").click();
      });
          $('#bs-example-navbar-collapse-1 .nav-locales').click(function() {
             $(".m-p-g__controls-close").click();

      });
           $('#bs-example-navbar-collapse-1 .nav-eventos').click(function() {
             $(".m-p-g__controls-close").click();

      });
            $('#bs-example-navbar-collapse-1 .nav-galeria').click(function() {
               $(".m-p-g__controls-close").click();

      });
             $('#bs-example-navbar-collapse-1 .nav-contacto').click(function() {
               $(".m-p-g__controls-close").click();
      })
      });
     
var App = (function () {

  //=== Use Strict ===//
  'use strict';

  //=== Private Variables ===//
  var gallery = $('#js-gallery');

  //=== Gallery Object ===//
  var Gallery = {
    switch: function(trigger, imgContainer) {
      var src = trigger.attr('href'),
      thumbs = trigger.siblings(),
      img = trigger.parent().prev().children();
      
      // Add active class to thumb
      trigger.addClass('is-active');
      
      // Remove active class from thumbs
      thumbs.each(function() {
        if( $(this).hasClass('is-active') ) {
          $(this).removeClass('is-active');
        }
      });

      // Switch image source
      img.attr('src', src);
    }
  };

  //=== Public Methods ===//
  function init() {

    // Listen for clicks on anchors within gallery
    gallery.delegate('a', 'click', function(event) {
      var trigger = $(this);
      var triggerData = trigger.data("gallery");

      if ( triggerData === 'zoom') {
        var imgContainer = trigger.parent(),
        img = trigger.siblings();
        Gallery.zoom(imgContainer, img);
      } else if ( triggerData === 'thumb') {
        var imgContainer = trigger.parent().siblings();
        Gallery.switch(trigger, imgContainer);
      } else {
        return;
      }

      event.preventDefault();
    });

  }

  //=== Make Methods Public ===//
  return {
    init: init
  };

})();

App.init();