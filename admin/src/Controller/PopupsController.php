<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Popups Controller
 *
 * @property \App\Model\Table\PopupsTable $Popups
 *
 * @method \App\Model\Entity\Popup[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PopupsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $popups = $this->paginate($this->Popups);

        $this->set(compact('popups'));
    }

    /**
     * View method
     *
     * @param string|null $id Popup id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $popup = $this->Popups->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('popup', $popup);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    { 
        $popup = $this->Popups->newEntity();
        if ($this->request->is('post')) {
            $popup = $this->Popups->patchEntity($popup, $this->request->getData());
            $popup->user_id = $this->Auth->user('id');
            if ($this->Popups->save($popup)) {
                $this->Flash->success(__('The popup has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The popup could not be saved. Please, try again.'));
        }
        $users = $this->Popups->Users->find('list', ['limit' => 200]);
        $this->set(compact('popup', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Popup id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $popup = $this->Popups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $popup = $this->Popups->patchEntity($popup, $this->request->getData());
            $popup->user_id = $this->Auth->user('id');
            if ($this->Popups->save($popup)) {
                $this->Flash->success(__('The popup has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The popup could not be saved. Please, try again.'));
        }
        $users = $this->Popups->Users->find('list', ['limit' => 200]);
        $this->set(compact('popup', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Popup id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $popup = $this->Popups->get($id);
        if ($this->Popups->delete($popup)) {
            $this->Flash->success(__('The popup has been deleted.'));
        } else {
            $this->Flash->error(__('The popup could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
