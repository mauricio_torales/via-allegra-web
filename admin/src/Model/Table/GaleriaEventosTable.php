<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GaleriaEventos Model
 *
 * @property \App\Model\Table\ActividadesTable|\Cake\ORM\Association\BelongsTo $Actividades
 *
 * @method \App\Model\Entity\GaleriaEvento get($primaryKey, $options = [])
 * @method \App\Model\Entity\GaleriaEvento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GaleriaEvento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GaleriaEvento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GaleriaEvento|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GaleriaEvento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GaleriaEvento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GaleriaEvento findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GaleriaEventosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('galeria_eventos');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Actividades', [
            'foreignKey' => 'evento_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [

            'foto' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto2' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto2_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto3' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto3_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto4' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto4_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto5' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto5_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto6' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto6_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto7' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto7_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto8' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto8_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto9' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto9_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto10' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto10_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto11' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto11_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto12' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto12_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto13' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto13_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto14' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto14_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto15' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto15_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

      


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['evento_id'], 'Actividades'));

        return $rules;
    }
}
