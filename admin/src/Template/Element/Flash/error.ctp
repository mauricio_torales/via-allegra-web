<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
echo "<script>Swal({
    type: 'error',
    title: 'Oops...',
    text: '$message'
  })</script>";
?>

