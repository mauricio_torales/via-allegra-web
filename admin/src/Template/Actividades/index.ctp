<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Actividade[]|\Cake\Collection\CollectionInterface $actividades
 */
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Eventos</li>
    <li class="breadcrumb-item active">Listado</li>
</ol>
<div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <?= $this->Html->link(
                            '<i class="icon-plus"></i>' . __('Agregar').'</a>',
                            ['controller' => 'Actividades','action' => 'add',],
                            ['escape' => false, 'class' => 'btn btn-success']
                        ) ?>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                <th scope="col"><?= $this->Paginator->sort('orden') ?></th>
                                <th>Evento</th>
                                <th scope="col" class="actions"><?= __('Acciones') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($actividades as $actividad): ?>
                                <tr>
                                    <td><?= $this->Number->format($actividad->orden) ?></td>
                                    <td><?= h($actividad->nombre) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('<i class="icon-picture"></i> ' .'Agregar album de fotos'), ['controller' => 'GaleriaEventos','action' => 'add'.'?id='.$actividad->id.'&ev='.$actividad->nombre],['escape' => false]) ?> &nbsp;&nbsp;
                                        <?= $this->Html->link(__('<i class="icon-note"></i> ' .'Editar'), ['action' => 'edit', $actividad->id],['escape' => false]) ?> &nbsp;&nbsp;

                                        <?php
                                         $actividad_id = $actividad->id;
                                         echo '<button onclick="eliminar('.$actividad_id.')" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>';
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <?= $this->Paginator->prev(__('Ant'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item active">
                                <?= $this->Paginator->first( __('Inicio'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->numbers(['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->next(__('Sig'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->last(__('Final'),['class'=>'page-link']) ?>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Fin ejemplo de tabla Listado -->
</div>
<script>
    function eliminar(name,) {
        Swal({
        title: 'Eliminar?',
        text: "Esta seguro de eliminar este evento y las fotos que contiene!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            location.href="actividades/delete/"+ name;
        }
        });
    };
</script>