<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Actividade $actividade
 */
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GaleriaEvento[]|\Cake\Collection\CollectionInterface $galeriaEventos
 */
?>

<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
   
   
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }

  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Eventos</li>
    <li class="breadcrumb-item ">Editar</li>
    <li class="breadcrumb-item active"><?= h($actividade->nombre) ?></li>
</ol>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <strong>Eventos</strong>
            <small>Editar</small>
        </div>
        <?= $this->Form->create($actividade,['type' => 'file']) ?>
        <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                    <label for="name">Orden</label>
                    <?php echo $this->Form->control('orden',['label' => false,'class'=>'form-control']);?>
                  </div>
                  <div class="form-group">
                    <label for="name">Nombre</label>
                    <?php echo $this->Form->control('nombre',['label' => false,'class'=>'form-control']);?>
                  </div>
                  <div class="form-group">
                    <label for="name">Nombre <span style="color:red;">Ingles</span></label>
                    <?php echo $this->Form->control('name',['label' => false,'class'=>'form-control']);?>
                  </div>
                  <div class="form-group">
                    <label for="name">Descripción</label>
                    <?php echo $this->Form->control('descripcion',['label' => false,'id'=>'editor']);?>
                    
                  </div>
                  <div class="form-group">
                    <label for="name">Descripción <span style="color:red;">Ingles</span></label>
                    <?php echo $this->Form->control('description',['label' => false,'id'=>'editor2']);?>
                  </div>
                  <div class="row" >
                      <div class="col-sm-12">
                        <center><label for="name">Vista previa</label></center>
                          <div class="box">
                                <?php 
                                $logo_old= $actividade->logo;
                                $logo_dir_old= $actividade->logo_dir;
                                echo '<div class="js--image-preview" style="background-image:url(https://linco.com.py/beta/via-allegra-web/admin/'.$logo_dir_old.''.$logo_old.');"></div>';
                                ?>
                              <div class="upload-options">
                              <label class="lbl-logo">
                                  <input type="file" id="logo" name="" class="image-upload" accept="image/*" />
                              </label>
                              </div>
                          </div>
                      </div>
                  </div>
              
              <center><label for="name">Album de fotos</label></center>
                        <table class="table table-bordered table-striped table-sm">
                              <thead>
                                  <tr>
                                  <th>Titulo</th>
                                  <th scope="col" class="actions"><?= __('Acciones') ?></th>
                                  </tr>
                              </thead>
                              <tbody>
                            
                                  <?php
                                      use Cake\ORM\TableRegistry;

                                      $album = TableRegistry::getTableLocator()->get('GaleriaEventos');

                                      $query = $album->find();

                                      foreach ($query as $row) {
                                        if ($row->evento_id==$actividade->id) {
                                  ?>
                              

                                  <tr>
                                      <?php 
                                        if (!empty($row->titulo)) {
                                          echo'<td>'.$row->titulo.' '.$row->year.'</td>';
                                        }else{
                                          echo"<td>Album de Fotos sin titulo</td>";
                                        }?>
                                      
                                      
                                      <td class="actions">
                                          <?= $this->Html->link(__('<i class="icon-note"></i> ' .'Editar'), ['controller' => 'GaleriaEventos','action' => 'edit', $row->id],['escape' => false]) ?> &nbsp;&nbsp;

                                          <?php
                                          echo '<button onclick="eliminar('.$row->id.')" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>';
                                          ?>
                                      </td>
                                  </tr>

                                  <?php }else{ ?>
                              
                                  <?php }} ?>
                            </tbody>
                        </table>
              </div>
              <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>

            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
   function eliminar(name,) {
        Swal({
        title: 'Eliminar?',
        text: "Esta seguro de eliminar este album!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            location.href="../../galeria-eventos/delete/"+ name;
        }
        });
    };

  $('.lbl-logo').click(function(){
      $('#logo').attr('name', 'logo');
  });

    function initImageUpload(box) {
      let uploadField = box.querySelector('.image-upload');

      uploadField.addEventListener('change', getFile);

      function getFile(e){
        let file = e.currentTarget.files[0];
        checkType(file);
      }
      
      function previewImage(file){
        let thumb = box.querySelector('.js--image-preview'),
            reader = new FileReader();

        reader.onload = function() {
          thumb.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(file);
        thumb.className += ' js--no-default';
      }

      function checkType(file){
        let imageType = /image.*/;
        if (!file.type.match(imageType)) {
          throw 'Datei ist kein Bild';
        } else if (!file){
          throw 'Kein Bild gewählt';
        } else {
          previewImage(file);
        }
      }
      
    }

    // initialize box-scope
    var boxes = document.querySelectorAll('.box');

    for (let i = 0; i < boxes.length; i++) {
      let box = boxes[i];
      initDropEffect(box);
      initImageUpload(box);
    }



    /// drop-effect
    function initDropEffect(box){
      let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
      
      // get clickable area for drop effect
      area = box.querySelector('.js--image-preview');
      area.addEventListener('click', fireRipple);
      
      function fireRipple(e){
        area = e.currentTarget
        // create drop
        if(!drop){
          drop = document.createElement('span');
          drop.className = 'drop';
          this.appendChild(drop);
        }
        // reset animate class
        drop.className = 'drop';
        
        // calculate dimensions of area (longest side)
        areaWidth = getComputedStyle(this, null).getPropertyValue("width");
        areaHeight = getComputedStyle(this, null).getPropertyValue("height");
        maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

        // set drop dimensions to fill area
        drop.style.width = maxDistance + 'px';
        drop.style.height = maxDistance + 'px';
        
        // calculate dimensions of drop
        dropWidth = getComputedStyle(this, null).getPropertyValue("width");
        dropHeight = getComputedStyle(this, null).getPropertyValue("height");
        
        // calculate relative coordinates of click
        // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
        x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
        y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
        
        // position drop and animate
        drop.style.top = y + 'px';
        drop.style.left = x + 'px';
        drop.className += ' animate';
        e.stopPropagation();
        
      }
    }
</script>