<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tienda[]|\Cake\Collection\CollectionInterface $tiendas
 */
?>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Locales</li>
    <li class="breadcrumb-item active">Listado</li>
</ol>
<div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <?= $this->Html->link(
                            '<i class="icon-plus"></i>' . __('Agregar').'</a>',
                            ['controller' => 'Tiendas','action' => 'add',],
                            ['escape' => false, 'class' => 'btn btn-success']
                        ) ?>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th>Local</th>
                                <th scope="col" class="actions"><?= __('Acciones') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($tiendas as $tienda): ?>
                                <tr>
                                    <td><?= $this->Number->format($tienda->id) ?></td>
                                    <td><?= h($tienda->nombre) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('<i class="icon-eye"></i> ' .'Ver'), ['action' => 'view', $tienda->id],['escape' => false]) ?>&nbsp;&nbsp; 
                                        <?= $this->Html->link(__('<i class="icon-note"></i> ' .'Editar'), ['action' => 'edit', $tienda->id],['escape' => false]) ?> &nbsp;&nbsp;
                                        <?php
                                         $tienda = $tienda->id;
                                         echo '<button onclick="eliminar('.$tienda.')" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>';
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <?= $this->Paginator->prev(__('Ant'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item active">
                                <?= $this->Paginator->first( __('Inicio'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->numbers(['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->next(__('Sig'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->last(__('Final'),['class'=>'page-link']) ?>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Fin ejemplo de tabla Listado -->
</div>
<script>
    function eliminar(name,) {
        Swal({
        title: 'Eliminar!',
        text: "Esta seguro de eliminar este local?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            location.href="tiendas/delete/"+ name;
        }
        });
    };
</script>
