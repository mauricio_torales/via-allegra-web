<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GaleriaEvento $galeriaEvento
 */
?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
    content: "photo_size_select_actual";
    font-family: 'Material Icons';
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }

  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Eventos</li>
    <li class="breadcrumb-item active">Agregar imagenes y video</li>
</ol>
<div class="col-sm-12">
    <?= $this->Form->create($galeriaEvento,['type' => 'file']) ?>
    <div class="card">
        <div class="card-header">
            <strong><?=$_GET["ev"] ?></strong>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <input style="display:none;" class="form-control" id="evento_id" name="evento_id" type="text" value="<?=$_GET["id"] ?>">
                        <label for="name">Titulo del album para el evento </label>
                        <input class="form-control" id="nombre" name="titulo" type="text" placeholder="Ingrese el nombre del evento" required>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <input style="display:none;" class="form-control" id="evento_id" name="evento_id" type="text" value="<?=$_GET["id"] ?>">
                        <label for="name">Titulo del album para el evento <span style="color:red;">en Ingles</span> </label>
                        <input class="form-control" id="nombre" name="title" type="text" placeholder="Ingrese el nombre del evento en ingles" required>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <input style="display:none;" class="form-control" id="evento_id" name="evento_id" type="text" value="<?=$_GET["id"] ?>">
                        <label for="name">Año </label>
                        <input class="form-control" id="year" name="year" type="text" placeholder="Ingrese el nombre del evento" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <small>Video</small>
        </div>
        <div class="card-body">
            <div class="row">
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name">Url del video en Youtube. <br>Ej:<span style="color:red;"> https://www.youtube.com/watch?v=IXdNnw99-Ic</span></label>
                        <input class="form-control" id="url" name="url" type="text" placeholder="codigo del video">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name">Miniatura o vista previa</label><br>
                        <img style="width:200px;" id="vista" src="">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="name">Video</label><br>
                        <iframe  id="video" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <small>Imagenes</small>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4 foto30" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto30">
                                <input type="file" id="foto30" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto29" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto29">
                                <input type="file" id="foto29" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto28" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto28">
                                <input type="file" id="foto28" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto27" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto27">
                                <input type="file" id="foto27" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto26" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto26">
                                <input type="file" id="foto26" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto25" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto25">
                                <input type="file" id="foto25" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto24" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto24">
                                <input type="file" id="foto24" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto23" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto23">
                                <input type="file" id="foto23" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto22" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto22">
                                <input type="file" id="foto22" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto21" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto21">
                                <input type="file" id="foto21" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto20" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto20">
                                <input type="file" id="foto20" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto19" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto19">
                                <input type="file" id="foto19" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto18" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto18">
                                <input type="file" id="foto18" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto17" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto17">
                                <input type="file" id="foto17" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto16" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto16">
                                <input type="file" id="foto16" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto15" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto15">
                                <input type="file" id="foto15" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto14" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto14">
                                <input type="file" id="foto14" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto13" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto13">
                                <input type="file" id="foto13" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto12" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto12">
                                <input type="file" id="foto12" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto11" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto11">
                                <input type="file" id="foto11" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto10" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto10">
                                <input type="file" id="foto10" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto9" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto9">
                                <input type="file" id="foto9" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                 <div class="col-sm-4 foto8" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto8">
                                <input type="file" id="foto8" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto7" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto7">
                                <input type="file" id="foto7" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto6" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto6">
                                <input type="file" id="foto6" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto5" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto5">
                                <input type="file" id="foto5" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto4" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto4">
                                <input type="file" id="foto4" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto3" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto3">
                                <input type="file" id="foto3" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 foto2" style="display:none;">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto2">
                                <input type="file" id="foto2" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="box">
                        <div class="js--image-preview"></div>
                        <div class="upload-options">
                            <label class="lbl-foto">
                                <input type="file" id="foto" name="" class="image-upload" accept="image/*" />
                            </label>
                        </div>
                    </div>
                </div>
             </div>
        </div>
        <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>
    </div>
    
    <?= $this->Form->end() ?>
    </div>
</div>
<script>
    $('#url').on('change', function(){

        var newval = '',
            $this = $(this);

        if (newval = $this.val().match(/(\?|&)v=([^&#]+)/)) {

            $this.val(newval.pop());

        } else if (newval = $this.val().match(/(\.be\/)+([^\/]+)/)) {

            $this.val(newval.pop());

        } else if (newval = $this.val().match(/(\embed\/)+([^\/]+)/)) {

            $this.val(newval.pop().replace('?rel=0',''));

        }

        });
    
    $('#url').blur(function() {
        var url = document.getElementById("url").value;
        $('#video').attr('src', 'https://www.youtube.com/embed/'+ url);
        $('#vista').attr('src', 'https://img.youtube.com/vi/'+ url+'/1.jpg'); 
    });

    $('.lbl-foto').click(function(){
        $('#foto').attr('name', 'foto');
        $( ".foto2" ).show();

    });
    $('.lbl-foto2').click(function(){
        $('#foto2').attr('name', 'foto2');
        $( ".foto3" ).show();

    });

    $('.lbl-foto3').click(function(){
        $('#foto3').attr('name', 'foto3');
        $( ".foto4" ).show();
    });
    $('.lbl-foto4').click(function(){
        $('#foto4').attr('name', 'foto4');
        $( ".foto5" ).show();
    });
    $('.lbl-foto5').click(function(){
        $('#foto5').attr('name', 'foto5');
        $( ".foto6" ).show();
    });
    $('.lbl-foto6').click(function(){
        $('#foto6').attr('name', 'foto6');
        $( ".foto7" ).show();
    });
    $('.lbl-foto7').click(function(){
        $('#foto7').attr('name', 'foto7');
        $( ".foto8" ).show();
    });
    $('.lbl-foto8').click(function(){
        $('#foto8').attr('name', 'foto8');
        $( ".foto9" ).show();
    });
    $('.lbl-foto9').click(function(){
        $('#foto9').attr('name', 'foto9');
        $( ".foto10" ).show();
    });
    $('.lbl-foto10').click(function(){
        $('#foto10').attr('name', 'foto10');
        $( ".foto11" ).show();
    });
    $('.lbl-foto11').click(function(){
        $('#foto11').attr('name', 'foto11');
        $( ".foto12" ).show();
    });
    $('.lbl-foto12').click(function(){
        $('#foto12').attr('name', 'foto12');
        $( ".foto13" ).show();
    });
    $('.lbl-foto13').click(function(){
        $('#foto13').attr('name', 'foto13');
        $( ".foto14" ).show();
    });
    $('.lbl-foto14').click(function(){
        $('#foto14').attr('name', 'foto14');
        $( ".foto15" ).show();
    });
    $('.lbl-foto15').click(function(){
        $('#foto15').attr('name', 'foto15');
        $( ".foto16" ).show();
    });
    $('.lbl-foto16').click(function(){
        $('#foto16').attr('name', 'foto16');
        $( ".foto17" ).show();
    });
    $('.lbl-foto17').click(function(){
        $('#foto17').attr('name', 'foto17');
        $( ".foto18" ).show();
    });
    $('.lbl-foto18').click(function(){
        $('#foto18').attr('name', 'foto18');
        $( ".foto19" ).show();
    });
    $('.lbl-foto19').click(function(){
        $('#foto19').attr('name', 'foto19');
        $( ".foto20" ).show();
    });
    $('.lbl-foto20').click(function(){
        $('#foto20').attr('name', 'foto20');
        $( ".foto21" ).show();
    });
    $('.lbl-foto21').click(function(){
        $('#foto21').attr('name', 'foto21');
        $( ".foto22" ).show();
    });
    $('.lbl-foto22').click(function(){
        $('#foto22').attr('name', 'foto22');
        $( ".foto23" ).show();
    });
    $('.lbl-foto23').click(function(){
        $('#foto23').attr('name', 'foto23');
        $( ".foto24" ).show();
    });
    $('.lbl-foto24').click(function(){
        $('#foto24').attr('name', 'foto24');
        $( ".foto25" ).show();
    });
    $('.lbl-foto25').click(function(){
        $('#foto25').attr('name', 'foto25');
        $( ".foto26" ).show();
    });
    $('.lbl-foto26').click(function(){
        $('#foto26').attr('name', 'foto26');
        $( ".foto27" ).show();
    });
    $('.lbl-foto27').click(function(){
        $('#foto27').attr('name', 'foto27');
        $( ".foto28" ).show();
    });
    $('.lbl-foto28').click(function(){
        $('#foto28').attr('name', 'foto28');
        $( ".foto29" ).show();
    });
    $('.lbl-foto29').click(function(){
        $('#foto29').attr('name', 'foto29');
        $( ".foto30" ).show();
    });
    $('.lbl-foto30').click(function(){
        $('#foto30').attr('name', 'foto30');
    });
    function initImageUpload(box) {
        let uploadField = box.querySelector('.image-upload');

        uploadField.addEventListener('change', getFile);

        function getFile(e){
        let file = e.currentTarget.files[0];
        checkType(file);
        }
        
        function previewImage(file){
        let thumb = box.querySelector('.js--image-preview'),
            reader = new FileReader();

        reader.onload = function() {
            thumb.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(file);
        thumb.className += ' js--no-default';
        }

        function checkType(file){
        let imageType = /image.*/;
        if (!file.type.match(imageType)) {
            throw 'Datei ist kein Bild';
        } else if (!file){
            throw 'Kein Bild gewählt';
        } else {
            previewImage(file);
        }
        }
        
    }

    // initialize box-scope
    var boxes = document.querySelectorAll('.box');

    for (let i = 0; i < boxes.length; i++) {
        let box = boxes[i];
        initDropEffect(box);
        initImageUpload(box);
    }



    /// drop-effect
    function initDropEffect(box){
        let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
        
        // get clickable area for drop effect
        area = box.querySelector('.js--image-preview');
        area.addEventListener('click', fireRipple);
        
        function fireRipple(e){
        area = e.currentTarget
        // create drop
        if(!drop){
            drop = document.createElement('span');
            drop.className = 'drop';
            this.appendChild(drop);
        }
        // reset animate class
        drop.className = 'drop';
        
        // calculate dimensions of area (longest side)
        areaWidth = getComputedStyle(this, null).getPropertyValue("width");
        areaHeight = getComputedStyle(this, null).getPropertyValue("height");
        maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

        // set drop dimensions to fill area
        drop.style.width = maxDistance + 'px';
        drop.style.height = maxDistance + 'px';
        
        // calculate dimensions of drop
        dropWidth = getComputedStyle(this, null).getPropertyValue("width");
        dropHeight = getComputedStyle(this, null).getPropertyValue("height");
        
        // calculate relative coordinates of click
        // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
        x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
        y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
        
        // position drop and animate
        drop.style.top = y + 'px';
        drop.style.left = x + 'px';
        drop.className += ' animate';
        e.stopPropagation();
        
        }
    }
</script>