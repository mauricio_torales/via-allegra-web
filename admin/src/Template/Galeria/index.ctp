<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Galerium[]|\Cake\Collection\CollectionInterface $galeria
 */
?>
<style>
.eliminar-td{
    display:none;
}
</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Galeria</li>
    <li class="breadcrumb-item active">Listado</li>
</ol>
<div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Imagenes de la Galeria
                        <?= $this->Html->link(
                            '<i class="icon-plus"></i>' . __('Agregar').'</a>',
                            ['controller' => 'Galeria','action' => 'add',],
                            ['escape' => false, 'class' => 'btn btn-success']
                        ) ?>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th>imagen</th>
                                <th>video</th>
                                <th scope="col" class="actions"><?= __('Eliminar') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($galeria as $galerium): ?>
                                <tr>
                                    <td><?= $this->Number->format($galerium->id) ?></td>
                                    <td> <?php
                                    if (!empty($galerium->foto)) {
                                        echo'<img style="width:100px;margin-left:100px;" src="https://linco.com.py/beta/via-allegra-web/admin/'.$galerium->foto_dir.''.$galerium->foto.'">';
                                    }
                                    ?> 
                                     </td>
                                    <td> <?php 
                                    if (!empty($galerium->url)) {
                                        echo'<img style="width:100px;margin-left:100px;" src="https://img.youtube.com/vi/'.$galerium->url.'/1.jpg">';
                                    }
                                    ?>
                                    </td>
                                    <td class="actions" style="padding:10px;">
                                        <?php
                                         $foto = $galerium->id;
                                         echo '<button onclick="eliminar('.$foto.')" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>';
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <?= $this->Paginator->prev(__('Ant'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item active">
                                <?= $this->Paginator->first( __('Inicio'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->numbers(['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->next(__('Sig'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->last(__('Final'),['class'=>'page-link']) ?>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Fin ejemplo de tabla Listado -->
</div>
<script>
    function eliminar(name,) {
        Swal({
        title: 'Eliminar?',
        text: "Esta seguro de eliminar esta imagen!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            location.href="galeria/delete/"+ name;
        }
        });
    };
</script>
