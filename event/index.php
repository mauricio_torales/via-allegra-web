<?php
include '../conexion.php';
$id = 'vacio';
if ( isset( $_GET['id'] ) ) {
  $id = $_GET['id'];
}
$url='https://linco.com.py/beta/via-allegra-web/admin/';
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Via Allegra</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel='stylesheet' href='https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/css/lightgallery.css'>
    <script src='https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js'></script>
    <script src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script>
   <style>
      .lg-toolbar{
        margin-top: 0px!important;
      }
      #lg-share{
        display: none!important;
      }
      .lg{
        margin-top: 100px!important;
        height: 90%!important;
      }
      

    </style>
  </head>
  <body> 
<header>
      <div class="container" style="margin-top:10px;">
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7">
            <a href="../home" rel="logo" class="">
              <img src="../images/logo.svg"  alt="Logo" class="img-responsive text-center logo">
            </a>
          </div>

              <nav class="navbar navbar-default wrap home">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <li >
                          <a  href="../home" rel="inicio">HOME</a> 
                        </li>
                        <li><a href="../home/#nosotros">ABOUT US</a></li>
                        <li><a href="../home/#locales">SHOPS</a></li>
                        <li><a class="active" href="../home/#eventos" >EVENTS</a></li>
                        <li><a href="../home/#galeria" >GALLERY</a></li>
                        <li><a href="../home/#contacto" >CONTACT</a></li>
                         <?php echo '<li><a href="../evento?id='.$id.'" rel=""><img src="../images/es.png" class="img-responsive idioma-inactivo"></a></li>';     ?>  
                        <?php echo '<li><a href="../event?id='.$id.'" rel=""><img src="../images/en.png" class="img-responsive idioma-activo"></a></li>';     ?>
                      
                      </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
              </nav>
          
      </div>
</header>

<section class="evento-cont">
   <br> <br><br><br><br>
  
    <div class="cabecera">
      <div class="container">
          <?php $sql = "SELECT * FROM actividades where id= '$id' ";
                  $con=1;
                 $result = $db->query($sql);
                  if ($result->num_rows > 0) {      
                    while($row = $result->fetch_assoc()) {
          echo'<h1 class="titulo-evento">'.$row["name"].'</h1>';
          
          echo'<div class="texto-evento">'.$row["description"].'</div>';

          }}?>
          <br>
          <center><div class="linea-evento"></div></center>
          <br>
      </div>
    </div>
    <div class="container">

    <?php $sql = "SELECT * FROM galeria_eventos where evento_id= '$id' order by year desc ";
      $result = $db->query($sql);
      if ($result->num_rows > 0) {      
        while($row = $result->fetch_assoc()) {

          echo'<h1 class="titulo-local">'.$row["title"].' '.$row["year"].'</h1>';?>

          <div class="row galerias">

            <div class="col-md-7" id="principal<?=$row["id"]?>">
              <?php if (!empty($row["url"])) { ?>
                
                <center>
                <iframe width="92%"  id="video<?=$row["id"]?>" src="https://www.youtube.com/embed/<?=$row["url"]?>?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </center>
                  <ul id="lightgallery<?=$row["id"]?>">
                    <?php if (!empty($row["foto"])) { ?>
                      <li id="img-principal<?=$row["id"]?>" style="display: none;" data-responsive="<?=$url.$row["foto_dir"].$row["foto"]?>" data-src="<?=$url.$row["foto_dir"].$row["foto"]?>">
                        <a href="">
                          <img  id="img-responsive<?=$row["id"]?>" class="img-responsive" src="<?=$url.$row["foto_dir"].$row["foto"]?>">
                        </a>
                      </li>
                    <?php }?>
                    <?php if (!empty($row["foto2"])) { ?>
                      <li  style="display: none;" data-responsive="<?=$url.$row["foto2_dir"].$row["foto2"]?>" data-src="<?=$url.$row["foto2_dir"].$row["foto2"]?>">
                        <a href="">
                          <img class="img-responsive" src="<?=$url.$row["foto2_dir"].$row["foto2"]?>">
                        </a>
                      </li>
                     <?php }?>
                    <?php if (!empty($row["foto3"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto3_dir"].$row["foto3"]?>" data-src="<?=$url.$row["foto3_dir"].$row["foto3"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto3_dir"].$row["foto3"]?>">
                      </a>
                    </li> 
                     <?php }?>
                    <?php if (!empty($row["foto4"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto4_dir"].$row["foto4"]?>" data-src="<?=$url.$row["foto4_dir"].$row["foto4"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto4_dir"].$row["foto4"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto5"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto5_dir"].$row["foto5"]?>" data-src="<?=$url.$row["foto5_dir"].$row["foto5"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto5_dir"].$row["foto5"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto6"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto6_dir"].$row["foto6"]?>" data-src="<?=$url.$row["foto6_dir"].$row["foto6"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto6_dir"].$row["foto6"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto7"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto7_dir"].$row["foto7"]?>" data-src="<?=$url.$row["foto7_dir"].$row["foto7"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto7_dir"].$row["foto7"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto8"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto8_dir"].$row["foto8"]?>" data-src="<?=$url.$row["foto8_dir"].$row["foto8"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto8_dir"].$row["foto8"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto9"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto9_dir"].$row["foto9"]?>" data-src="<?=$url.$row["foto9_dir"].$row["foto9"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto9_dir"].$row["foto9"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto10"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto10_dir"].$row["foto10"]?>" data-src="<?=$url.$row["foto10_dir"].$row["foto10"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto10_dir"].$row["foto10"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto11"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto11_dir"].$row["foto11"]?>" data-src="<?=$url.$row["foto11_dir"].$row["foto11"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto11_dir"].$row["foto11"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto12"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto12_dir"].$row["foto12"]?>" data-src="<?=$url.$row["foto12_dir"].$row["foto12"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto12_dir"].$row["foto12"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto13"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto13_dir"].$row["foto13"]?>" data-src="<?=$url.$row["foto13_dir"].$row["foto13"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto13_dir"].$row["foto13"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto14"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto14_dir"].$row["foto14"]?>" data-src="<?=$url.$row["foto14_dir"].$row["foto14"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto14_dir"].$row["foto14"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto15"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto15_dir"].$row["foto15"]?>" data-src="<?=$url.$row["foto15_dir"].$row["foto15"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto15_dir"].$row["foto15"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto16"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto16_dir"].$row["foto16"]?>" data-src="<?=$url.$row["foto16_dir"].$row["foto16"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto16_dir"].$row["foto16"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto17"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto17_dir"].$row["foto17"]?>" data-src="<?=$url.$row["foto17_dir"].$row["foto17"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto17_dir"].$row["foto17"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto18"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto18_dir"].$row["foto18"]?>" data-src="<?=$url.$row["foto18_dir"].$row["foto18"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto18_dir"].$row["foto18"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto19"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto19_dir"].$row["foto19"]?>" data-src="<?=$url.$row["foto19_dir"].$row["foto19"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto19_dir"].$row["foto19"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto20"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto20_dir"].$row["foto20"]?>" data-src="<?=$url.$row["foto20_dir"].$row["foto20"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto20_dir"].$row["foto20"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto21"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto21_dir"].$row["foto21"]?>" data-src="<?=$url.$row["foto21_dir"].$row["foto21"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto21_dir"].$row["foto21"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto22"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto22_dir"].$row["foto22"]?>" data-src="<?=$url.$row["foto22_dir"].$row["foto22"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto22_dir"].$row["foto22"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto23"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto23_dir"].$row["foto23"]?>" data-src="<?=$url.$row["foto23_dir"].$row["foto23"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto23_dir"].$row["foto23"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto24"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto24_dir"].$row["foto24"]?>" data-src="<?=$url.$row["foto24_dir"].$row["foto24"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto24_dir"].$row["foto24"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto25"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto25_dir"].$row["foto25"]?>" data-src="<?=$url.$row["foto25_dir"].$row["foto25"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto25_dir"].$row["foto25"]?>">
                      </a>
                     </li> 
                     <?php }?>

                      <?php if (!empty($row["foto26"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto26_dir"].$row["foto26"]?>" data-src="<?=$url.$row["foto26_dir"].$row["foto26"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto26_dir"].$row["foto26"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto27"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto27_dir"].$row["foto27"]?>" data-src="<?=$url.$row["foto27_dir"].$row["foto27"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto27_dir"].$row["foto27"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto28"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto28_dir"].$row["foto28"]?>" data-src="<?=$url.$row["foto28_dir"].$row["foto28"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto28_dir"].$row["foto28"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto29"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto29_dir"].$row["foto29"]?>" data-src="<?=$url.$row["foto29_dir"].$row["foto29"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto29_dir"].$row["foto29"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto30"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto30_dir"].$row["foto30"]?>" data-src="<?=$url.$row["foto30_dir"].$row["foto30"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto30_dir"].$row["foto30"]?>">
                      </a>
                     </li> 
                     <?php }?>
                  </ul>
              <?php }else{?>

                  <ul id="lightgallery<?=$row["id"]?>">

                    <li id="img-principal<?=$row["id"]?>" data-responsive="<?=$url.$row["foto_dir"].$row["foto"]?>" data-src="<?=$url.$row["foto_dir"].$row["foto"]?>">
                      <a href="">
                        <img  id="img-responsive<?=$row["id"]?>" class="img-responsive" src="<?=$url.$row["foto_dir"].$row["foto"]?>">
                      </a>
                    </li>
                    <?php if (!empty($row["foto2"])) { ?>
                      <li  style="display: none;" data-responsive="<?=$url.$row["foto2_dir"].$row["foto2"]?>" data-src="<?=$url.$row["foto2_dir"].$row["foto2"]?>">
                        <a href="">
                          <img class="img-responsive" src="<?=$url.$row["foto2_dir"].$row["foto2"]?>">
                        </a>
                      </li>
                     <?php }?>
                    <?php if (!empty($row["foto3"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto3_dir"].$row["foto3"]?>" data-src="<?=$url.$row["foto3_dir"].$row["foto3"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto3_dir"].$row["foto3"]?>">
                      </a>
                    </li> 
                     <?php }?>
                    <?php if (!empty($row["foto4"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto4_dir"].$row["foto4"]?>" data-src="<?=$url.$row["foto4_dir"].$row["foto4"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto4_dir"].$row["foto4"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto5"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto5_dir"].$row["foto5"]?>" data-src="<?=$url.$row["foto5_dir"].$row["foto5"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto5_dir"].$row["foto5"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto6"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto6_dir"].$row["foto6"]?>" data-src="<?=$url.$row["foto6_dir"].$row["foto6"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto6_dir"].$row["foto6"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto7"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto7_dir"].$row["foto7"]?>" data-src="<?=$url.$row["foto7_dir"].$row["foto7"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto7_dir"].$row["foto7"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto8"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto8_dir"].$row["foto8"]?>" data-src="<?=$url.$row["foto8_dir"].$row["foto8"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto8_dir"].$row["foto8"]?>">
                      </a>
                     </li> 
                     <?php }?>
                    <?php if (!empty($row["foto9"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto9_dir"].$row["foto9"]?>" data-src="<?=$url.$row["foto9_dir"].$row["foto9"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto9_dir"].$row["foto9"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto10"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto10_dir"].$row["foto10"]?>" data-src="<?=$url.$row["foto10_dir"].$row["foto10"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto10_dir"].$row["foto10"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto11"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto11_dir"].$row["foto11"]?>" data-src="<?=$url.$row["foto11_dir"].$row["foto11"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto11_dir"].$row["foto11"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto12"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto12_dir"].$row["foto12"]?>" data-src="<?=$url.$row["foto12_dir"].$row["foto12"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto12_dir"].$row["foto12"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto13"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto13_dir"].$row["foto13"]?>" data-src="<?=$url.$row["foto13_dir"].$row["foto13"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto13_dir"].$row["foto13"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto14"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto14_dir"].$row["foto14"]?>" data-src="<?=$url.$row["foto14_dir"].$row["foto14"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto14_dir"].$row["foto14"]?>">
                      </a>
                     </li> 
                     <?php }?>
                     <?php if (!empty($row["foto15"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto15_dir"].$row["foto15"]?>" data-src="<?=$url.$row["foto15_dir"].$row["foto15"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto15_dir"].$row["foto15"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto16"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto16_dir"].$row["foto16"]?>" data-src="<?=$url.$row["foto16_dir"].$row["foto16"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto16_dir"].$row["foto16"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto17"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto17_dir"].$row["foto17"]?>" data-src="<?=$url.$row["foto17_dir"].$row["foto17"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto17_dir"].$row["foto17"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto18"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto18_dir"].$row["foto18"]?>" data-src="<?=$url.$row["foto18_dir"].$row["foto18"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto18_dir"].$row["foto18"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto19"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto19_dir"].$row["foto19"]?>" data-src="<?=$url.$row["foto19_dir"].$row["foto19"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto19_dir"].$row["foto19"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto20"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto20_dir"].$row["foto20"]?>" data-src="<?=$url.$row["foto20_dir"].$row["foto20"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto20_dir"].$row["foto20"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto21"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto21_dir"].$row["foto21"]?>" data-src="<?=$url.$row["foto21_dir"].$row["foto21"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto21_dir"].$row["foto21"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto22"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto22_dir"].$row["foto22"]?>" data-src="<?=$url.$row["foto22_dir"].$row["foto22"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto22_dir"].$row["foto22"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto23"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto23_dir"].$row["foto23"]?>" data-src="<?=$url.$row["foto23_dir"].$row["foto23"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto23_dir"].$row["foto23"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto24"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto24_dir"].$row["foto24"]?>" data-src="<?=$url.$row["foto24_dir"].$row["foto24"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto24_dir"].$row["foto24"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto25"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto25_dir"].$row["foto25"]?>" data-src="<?=$url.$row["foto25_dir"].$row["foto25"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto25_dir"].$row["foto25"]?>">
                      </a>
                     </li> 
                     <?php }?>

                      <?php if (!empty($row["foto26"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto26_dir"].$row["foto26"]?>" data-src="<?=$url.$row["foto26_dir"].$row["foto26"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto26_dir"].$row["foto26"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto27"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto27_dir"].$row["foto27"]?>" data-src="<?=$url.$row["foto27_dir"].$row["foto27"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto27_dir"].$row["foto27"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto28"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto28_dir"].$row["foto28"]?>" data-src="<?=$url.$row["foto28_dir"].$row["foto28"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto28_dir"].$row["foto28"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto29"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto29_dir"].$row["foto29"]?>" data-src="<?=$url.$row["foto29_dir"].$row["foto29"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto29_dir"].$row["foto29"]?>">
                      </a>
                     </li> 
                     <?php }?>
                      <?php if (!empty($row["foto30"])) { ?>
                     <li  style="display: none;" data-responsive="<?=$url.$row["foto30_dir"].$row["foto30"]?>" data-src="<?=$url.$row["foto30_dir"].$row["foto30"]?>">
                      <a href="">
                        <img class="img-responsive" src="<?=$url.$row["foto30_dir"].$row["foto30"]?>">
                      </a>
                     </li> 
                     <?php }?>
                  </ul>
               
               
              <?php }?>
              <script>
                      lightGallery(document.getElementById('lightgallery<?=$row["id"]?>'))
              </script>
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-3">
              <div class="row miniaturas ">
                <?php 
                    if (!empty($row["url"])) { ?>
                   
                        <img class="video-alb<?=$row["id"]?> activo" src="https://img.youtube.com/vi/<?=$row["url"]?>/1.jpg">
                         <script>
                            $(".video-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).show();
                                $( "#img-principal<?=$row["id"]?>" ).hide();
                                $( ".video-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                    
                <?php }?>
                <?php 
                    if (!empty($row["foto"])) { ?>
                   
                        <img class="foto-alb<?=$row["id"]?>" src="<?=$url.$row["foto_dir"].$row["foto"]?>">
                        <script>
                            $(".foto-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto_dir"].$row["foto"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto_dir"].$row["foto"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto_dir"].$row["foto"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto_dir"].$row["foto"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto_dir"].$row["foto"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                 $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                               
                            });
                           
                        </script>
                      
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto2"])) { ?>
                  
                        <img class="foto2-alb<?=$row["id"]?>" src="<?=$url.$row["foto2_dir"].$row["foto2"]?>">
                        <script>
                            $(".foto2-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto2_dir"].$row["foto2"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto2_dir"].$row["foto2"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto2_dir"].$row["foto2"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto2_dir"].$row["foto2"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto2_dir"].$row["foto2"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });

                        </script>
                   

                <?php }?>
                <?php 
                    if (!empty($row["foto3"])) { ?>
                    
                        <img class="foto3-alb<?=$row["id"]?>" src="<?=$url.$row["foto3_dir"].$row["foto3"]?>">
                        <script>
                            $(".foto3-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto3_dir"].$row["foto3"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto3_dir"].$row["foto3"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto3_dir"].$row["foto3"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto3_dir"].$row["foto3"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto3_dir"].$row["foto3"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                    
                <?php }?>
                <?php 
                    if (!empty($row["foto4"])) { ?>
                    
                        <img class="foto4-alb<?=$row["id"]?>" src="<?=$url.$row["foto4_dir"].$row["foto4"]?>">
                        <script>
                            $(".foto4-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto4_dir"].$row["foto4"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto4_dir"].$row["foto4"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto4_dir"].$row["foto4"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto4_dir"].$row["foto4"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto4_dir"].$row["foto4"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                 $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto5"])) { ?>
                    
                        <img class="foto5-alb<?=$row["id"]?>" src="<?=$url.$row["foto5_dir"].$row["foto5"]?>">
                        <script>
                            $(".foto5-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto5_dir"].$row["foto5"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto5_dir"].$row["foto5"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto5_dir"].$row["foto5"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto5_dir"].$row["foto5"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto5_dir"].$row["foto5"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                 $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                    
                <?php }?>
                <?php 
                    if (!empty($row["foto6"])) { ?>
                    
                        <img class="foto6-alb<?=$row["id"]?>" src="<?=$url.$row["foto6_dir"].$row["foto6"]?>">
                        <script>
                            $(".foto6-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto6_dir"].$row["foto6"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto6_dir"].$row["foto6"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto6_dir"].$row["foto6"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto6_dir"].$row["foto6"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto6_dir"].$row["foto6"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                 $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto7"])) { ?>
                    
                        <img class="foto7-alb<?=$row["id"]?>" src="<?=$url.$row["foto7_dir"].$row["foto7"]?>">
                        <script>
                            $(".foto7-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto7_dir"].$row["foto7"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto7_dir"].$row["foto7"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto7_dir"].$row["foto7"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto7_dir"].$row["foto7"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto7_dir"].$row["foto7"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                 $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                    
                <?php }?>
                 <?php 
                    if (!empty($row["foto8"])) { ?>
                    
                        <img class="foto8-alb<?=$row["id"]?>" src="<?=$url.$row["foto8_dir"].$row["foto8"]?>">
                        <script>
                            $(".foto8-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto8_dir"].$row["foto8"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto8_dir"].$row["foto8"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto8_dir"].$row["foto8"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto8_dir"].$row["foto8"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto8_dir"].$row["foto8"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                 $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto9"])) { ?>
                    
                        <img class="foto9-alb<?=$row["id"]?>" src="<?=$url.$row["foto9_dir"].$row["foto9"]?>">
                        <script>
                            $(".foto9-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto9_dir"].$row["foto9"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto9_dir"].$row["foto9"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto9_dir"].$row["foto9"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto9_dir"].$row["foto9"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto9_dir"].$row["foto9"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto10"])) { ?>
                    
                        <img class="foto10-alb<?=$row["id"]?>" src="<?=$url.$row["foto10_dir"].$row["foto10"]?>">
                        <script>
                            $(".foto10-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto10_dir"].$row["foto10"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto10_dir"].$row["foto10"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto10_dir"].$row["foto10"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto10_dir"].$row["foto10"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto10_dir"].$row["foto10"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto11"])) { ?>
                    
                        <img class="foto11-alb<?=$row["id"]?>" src="<?=$url.$row["foto11_dir"].$row["foto11"]?>">
                        <script>
                            $(".foto11-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto11_dir"].$row["foto11"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto11_dir"].$row["foto11"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto11_dir"].$row["foto11"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto11_dir"].$row["foto11"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto11_dir"].$row["foto11"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto12"])) { ?>
                    
                        <img class="foto12-alb<?=$row["id"]?>" src="<?=$url.$row["foto12_dir"].$row["foto12"]?>">
                        <script>
                            $(".foto12-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto12_dir"].$row["foto12"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto12_dir"].$row["foto12"]?>");
                                 $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto12_dir"].$row["foto12"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto12_dir"].$row["foto12"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto12_dir"].$row["foto12"]?>");


                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );

                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto13"])) { ?>
                    
                        <img class="foto13-alb<?=$row["id"]?>" src="<?=$url.$row["foto13_dir"].$row["foto13"]?>">
                        <script>
                            $(".foto13-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto13_dir"].$row["foto13"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto13_dir"].$row["foto13"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto13_dir"].$row["foto13"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto13_dir"].$row["foto13"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto13_dir"].$row["foto13"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto14"])) { ?>
                    
                        <img class="foto14-alb<?=$row["id"]?>" src="<?=$url.$row["foto14_dir"].$row["foto14"]?>">
                        <script>
                            $(".foto14-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto14_dir"].$row["foto14"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto14_dir"].$row["foto14"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto14_dir"].$row["foto14"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto14_dir"].$row["foto14"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto14_dir"].$row["foto14"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                 <?php 
                    if (!empty($row["foto15"])) { ?>
                    
                        <img class="foto15-alb<?=$row["id"]?>" src="<?=$url.$row["foto15_dir"].$row["foto15"]?>">
                        <script>
                            $(".foto15-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto15_dir"].$row["foto15"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto15_dir"].$row["foto15"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto15_dir"].$row["foto15"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto15_dir"].$row["foto15"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto15_dir"].$row["foto15"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto16"])) { ?>
                    
                        <img class="foto16-alb<?=$row["id"]?>" src="<?=$url.$row["foto16_dir"].$row["foto16"]?>">
                        <script>
                            $(".foto16-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto16_dir"].$row["foto16"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto16_dir"].$row["foto16"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto16_dir"].$row["foto16"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto16_dir"].$row["foto16"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto16_dir"].$row["foto16"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto17"])) { ?>
                    
                        <img class="foto17-alb<?=$row["id"]?>" src="<?=$url.$row["foto17_dir"].$row["foto17"]?>">
                        <script>
                            $(".foto17-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto17_dir"].$row["foto17"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto17_dir"].$row["foto17"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto17_dir"].$row["foto17"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto17_dir"].$row["foto17"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto17_dir"].$row["foto17"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto18"])) { ?>
                    
                        <img class="foto18-alb<?=$row["id"]?>" src="<?=$url.$row["foto18_dir"].$row["foto18"]?>">
                        <script>
                            $(".foto18-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto18_dir"].$row["foto18"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto18_dir"].$row["foto18"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto18_dir"].$row["foto18"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto18_dir"].$row["foto18"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto18_dir"].$row["foto18"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto19"])) { ?>
                    
                        <img class="foto19-alb<?=$row["id"]?>" src="<?=$url.$row["foto19_dir"].$row["foto19"]?>">
                        <script>
                            $(".foto19-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto19_dir"].$row["foto19"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto19_dir"].$row["foto19"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto19_dir"].$row["foto19"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto19_dir"].$row["foto19"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto19_dir"].$row["foto19"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto20"])) { ?>
                    
                        <img class="foto20-alb<?=$row["id"]?>" src="<?=$url.$row["foto20_dir"].$row["foto20"]?>">
                        <script>
                            $(".foto20-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto20_dir"].$row["foto20"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto20_dir"].$row["foto20"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto20_dir"].$row["foto20"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto20_dir"].$row["foto20"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto20_dir"].$row["foto20"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto21"])) { ?>
                    
                        <img class="foto21-alb<?=$row["id"]?>" src="<?=$url.$row["foto21_dir"].$row["foto21"]?>">
                        <script>
                            $(".foto21-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto21_dir"].$row["foto21"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto21_dir"].$row["foto21"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto21_dir"].$row["foto21"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto21_dir"].$row["foto21"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto21_dir"].$row["foto21"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto22"])) { ?>
                    
                        <img class="foto22-alb<?=$row["id"]?>" src="<?=$url.$row["foto22_dir"].$row["foto22"]?>">
                        <script>
                            $(".foto22-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto22_dir"].$row["foto22"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto22_dir"].$row["foto22"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto22_dir"].$row["foto22"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto22_dir"].$row["foto22"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto22_dir"].$row["foto22"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto23"])) { ?>
                    
                        <img class="foto23-alb<?=$row["id"]?>" src="<?=$url.$row["foto23_dir"].$row["foto23"]?>">
                        <script>
                            $(".foto23-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto23_dir"].$row["foto23"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto23_dir"].$row["foto23"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto23_dir"].$row["foto23"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto23_dir"].$row["foto23"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto23_dir"].$row["foto23"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto24"])) { ?>
                    
                        <img class="foto24-alb<?=$row["id"]?>" src="<?=$url.$row["foto24_dir"].$row["foto24"]?>">
                        <script>
                            $(".foto24-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto24_dir"].$row["foto24"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto24_dir"].$row["foto24"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto24_dir"].$row["foto24"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto24_dir"].$row["foto24"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto24_dir"].$row["foto24"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto25"])) { ?>
                    
                        <img class="foto25-alb<?=$row["id"]?>" src="<?=$url.$row["foto25_dir"].$row["foto25"]?>">
                        <script>
                            $(".foto25-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto25_dir"].$row["foto25"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto25_dir"].$row["foto25"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto25_dir"].$row["foto25"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto25_dir"].$row["foto25"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto25_dir"].$row["foto25"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto26"])) { ?>
                    
                        <img class="foto26-alb<?=$row["id"]?>" src="<?=$url.$row["foto26_dir"].$row["foto26"]?>">
                        <script>
                            $(".foto26-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto26_dir"].$row["foto26"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto26_dir"].$row["foto26"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto26_dir"].$row["foto26"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto26_dir"].$row["foto26"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto26_dir"].$row["foto26"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto27"])) { ?>
                    
                        <img class="foto27-alb<?=$row["id"]?>" src="<?=$url.$row["foto27_dir"].$row["foto27"]?>">
                        <script>
                            $(".foto27-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto27_dir"].$row["foto27"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto27_dir"].$row["foto27"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto27_dir"].$row["foto27"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto27_dir"].$row["foto27"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto27_dir"].$row["foto27"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto28"])) { ?>
                    
                        <img class="foto28-alb<?=$row["id"]?>" src="<?=$url.$row["foto28_dir"].$row["foto28"]?>">
                        <script>
                            $(".foto28-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto28_dir"].$row["foto28"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto28_dir"].$row["foto28"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto28_dir"].$row["foto28"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto28_dir"].$row["foto28"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto28_dir"].$row["foto28"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto29"])) { ?>
                    
                        <img class="foto29-alb<?=$row["id"]?>" src="<?=$url.$row["foto29_dir"].$row["foto29"]?>">
                        <script>
                            $(".foto29-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto29_dir"].$row["foto29"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto29_dir"].$row["foto29"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto29_dir"].$row["foto29"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto29_dir"].$row["foto29"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto29_dir"].$row["foto29"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
                <?php 
                    if (!empty($row["foto30"])) { ?>
                    
                        <img class="foto30-alb<?=$row["id"]?>" src="<?=$url.$row["foto30_dir"].$row["foto30"]?>">
                        <script>
                            $(".foto30-alb<?=$row["id"]?>").click(function(){
                                $( "#video<?=$row["id"]?>" ).hide();
                                $( "#img-principal<?=$row["id"]?>" ).show();

                                $("#img-principal<?=$row["id"]?>").attr("data-responsive", "<?=$url.$row["foto30_dir"].$row["foto30"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-src", "<?=$url.$row["foto30_dir"].$row["foto30"]?>");
                                $("#img-principal<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto30_dir"].$row["foto30"]?>");
                                $("#img-responsive<?=$row["id"]?>").attr("src", "<?=$url.$row["foto30_dir"].$row["foto30"]?>");
                                 $("#img-responsive<?=$row["id"]?>").attr("data-full", "<?=$url.$row["foto30_dir"].$row["foto30"]?>");

                                $( ".video-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto2-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto3-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto4-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto5-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto6-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto7-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto8-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto9-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto10-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto15-alb<?=$row["id"]?>" ).addClass( "activo" );
                                $( ".foto12-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto13-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto11-alb<?=$row["id"]?>" ).removeClass( "activo" );
                                $( ".foto14-alb<?=$row["id"]?>" ).removeClass( "activo" );
                            });
                        </script>
                   
                <?php }?>
              </div>

            </div>

          </div>   
          <div class="clearfix"></div> 

    <?php }}?>



  <br><br><br>
    <center><a class="volver" href="../home/#eventos"><span><i class="fas fa-home"></span></i> back</a></center>


  </div>
  </div>

</section>

<footer>
<center>
  <p>Senador Long esq. Tte. Vera | Asunción, Paraguay</p>
  <p><a style="color: black; text-decoration: none;"  href="tel:+59521600077">+ 595 21 600077</a ></p>
   <div class="redes row-centered">
      <a class="facebook" href="https://www.facebook.com/PaseoViaAllegra/"><i class="fab fa-facebook-f"></i></a>
      <a class="twitter" href="https://www.twitter.com/ViaAllegraPy"><i class="fab fa-twitter"></i></a>
      <a class="instagram" href="https://www.instagram/PaseoViaAllegra"><i class="fab fa-instagram"></i></a>
    </div>
</center>
</footer>
<div class="firma">
<center >developed by <a href=""><img style="width: 35px;" src="../images/linco.svg"></a> </center>
</div>
  </body>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/index.js"></script>
   
   

</html>