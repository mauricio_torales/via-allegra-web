<?php
include '../conexion.php';
$id = 'vacio';
$url='https://linco.com.py/beta/via-allegra-web/admin/';
if ( isset( $_GET['id'] ) ) {
  $id = $_GET['id'];
}


?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Via Allegra</title>
 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/style.css">

  </head>
  <body> 
<header>
      <div class="container" style="margin-top:10px;">
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7">
            <a href="../home" rel="logo" class="">
              <img src="../images/logo.svg"  alt="Logo" class="img-responsive text-center logo">
            </a>
          </div>


              <nav class="navbar navbar-default wrap home">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <li >
                          <a  href="../home/" rel="home">HOME</a> 
                        </li>
                        <li><a href="../home/#nosotros">ABOUT US</a></li>
                        <li><a class="active" href="../home/#locales">SHOPS</a></li>
                        <li><a href="../home/#eventos" >EVENTS</a></li>
                        <li><a href="../home/#galeria" >GALLERY</a></li>
                        <li><a href="../home/#contacto" >CONTACT</a></li>
                  <?php echo '<li><a href="../local?id='.$id.'" rel=""><img src="../images/es.png" class="img-responsive idioma-inactivo"></a></li>';     ?>
                  <?php echo '<li><a href="../store?id='.$id.'" rel=""><img src="../images/en.png" class="img-responsive idioma-activo"></a></li>';     ?>  
                      </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
              </nav>

          
      </div>
</header>
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="4000">


      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php
            $cont=1;
            $sql = "SELECT * FROM tiendas where id='$id'";
              $result = $db->query($sql);
               if ($result->num_rows > 0) {      
                while($row = $result->fetch_assoc()) {
                    
                          echo'
                          <div class="item active">
                            <img src="'.$url.''.$row["foto_dir"].''.$row["foto"].'" style="width:100%;">
                          </div>
                          ';
                          
                          if (!empty($row["foto2"])) {
                            echo'
                                <div class="item">
                                  <img src="'.$url.''.$row["foto2_dir"].''.$row["foto2"].'" style="width:100%;">
                                </div>
                                ';
                          }
                         
                          if (!empty($row["foto3"])) {
                            echo'
                                <div class="item">
                                  <img src="'.$url.''.$row["foto3_dir"].''.$row["foto3"].'" style="width:100%;">
                                </div>
                                ';
                          }
                           if (!empty($row["foto4"])) {
                            echo'
                                <div class="item">
                                  <img src="'.$url.''.$row["foto4_dir"].''.$row["foto4"].'" style="width:100%;">
                                </div>
                                ';
                          }
                           if (!empty($row["foto5"])) {
                            echo'
                                <div class="item">
                                  <img src="'.$url.''.$row["foto5_dir"].''.$row["foto5"].'" style="width:100%;">
                                </div>
                                ';
                          }
                        ?>

      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
<section class="local">

<div class="container">
  <?php echo '<h1 class="titulo-local">'.$row["name"].'</h1>';?>
  <?php echo ' <div class="texto-local">'.$row["description"].'</div>';?>
  <h3 class="datos-local">CONTACT INFORMATION</h3>
  <div class="redes-local">
 <?php
    $horario=$row["hours"];
    $telefono=$row["telefono"];
    $email=$row["email"];
    $facebook=$row["facebook"];
    $instagram=$row["instagram"];
    $twitter=$row["twitter"];
    $web=$row["web"];
    if (!empty($horario)) {
       echo'<p class="cel"><span class="icono-mail"><i class="far fa-clock"></i></span>&nbsp;&nbsp;' .$horario.'</p>';
    }

    if (!empty($telefono)) {
       echo'<p class="cel"><span class="icono-cel"><a style="color:#303030;text-decoration:none;" href="tel:'.$telefono.'"><i class="fas fa-mobile-alt"></i></a></span><a style="color:#303030;text-decoration:none;" href="tel:'.$telefono.'">&nbsp;&nbsp;'.$telefono.'</a></p>';
    }
    if (!empty($email)) {
      echo'<p class="mail"><span class="icono-mail"><a style="color:#303030;text-decoration:none;" href="mailto:'.$email.'"><i class="far fa-envelope"></i></a></span><a style="color:#303030;text-decoration:none;" href="mailto:'.$email.'">&nbsp;&nbsp;'.$email.'</a></p>';
    }
    if (!empty($facebook)) {
      echo'<p class="face"><span class="icono-face"><a style="color:#303030;text-decoration:none;" href="https://'.$facebook.'"><i class="fab fa-facebook-f"></i></span>&nbsp;&nbsp;'.$facebook.'</a></p>';
    }
    if (!empty($instagram)) {
      echo'<p class="insta"><span class="icono-insta"><a style="color:#303030;text-decoration:none;" href="https://'.$instagram.'"><i class="fab fa-instagram"></i></a></span><a style="color:#303030;text-decoration:none;" href="https://'.$instagram.'">&nbsp;&nbsp;'.$instagram.'</a></p>';
    }
    if (!empty($twitter)) {
     echo'<p class="twit"><span class="icono-mail"><a style="color:#303030;text-decoration:none;" href="https://'.$twitter.'"><i class="fab fa-twitter"></i></span>&nbsp;&nbsp;'.$twitter.'</a></p>';
    }
    if (!empty($web)) {
      echo'<p class="twit"><span class="icono-mail"><i class="fas fa-globe"><a style="color:#303030;text-decoration:none;" href="http://'.$web.'"></i></span>&nbsp;&nbsp;'.$web.'</a></p>';
    }  
}} ?>
  </div>


<center><a class="volver" href="../home/#locales"><span><i class="fas fa-home"></span></i> 
return</a></center>
</div>
</section>

<footer>
<center>
  <p>Senador Long esq. Tte. Vera | Asunción, Paraguay</p>
  <p><a style="color: black; text-decoration: none;"  href="tel:+59521600077">+ 595 21 600077</a ></p>
   <div class="redes row-centered">
      <a class="facebook" href="https://www.facebook.com/PaseoViaAllegra/"><i class="fab fa-facebook-f"></i></a>
      <a class="twitter" href="https://www.twitter.com/ViaAllegraPy"><i class="fab fa-twitter"></i></a>
      <a class="instagram" href="https://www.instagram/PaseoViaAllegra"><i class="fab fa-instagram"></i></a>
    </div>
</center>
</footer>
<div class="firma">
<center >Developed by <a href=""><img style="width: 35px;" src="../images/linco.svg"></a> </center>
</div>
</body>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>