<?php
include '../conexion.php';
$url='https://linco.com.py/beta/via-allegra-web/admin/';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="Description" content="Marcas internacionales y nacionales componen este espacio donde el buen gusto y la sofisticación se encuentran en cada rincón. ">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
    <meta name="theme-color" content="#ffffff">


    <title>Via Allegra</title>
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/style.css">

    <link rel='stylesheet' href='https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/css/lightgallery.css'>
    <script src='https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js'></script>
    <script src='https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js'></script>
    <script src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script>
   <style>
      .galeria-p ul li{
        list-style: none!important;
        text-decoration: none!important;
      }
      .galeria-p ul li img{
        max-height: 400px;
        display: block;
        margin: auto;
      }
      .lg-toolbar{
        margin-top: 0px!important;
      }
      #lg-share{
        display: none!important;
      }
      .lg{
        margin-top: 100px!important;
        height: 90%!important;
      }
      

    </style>

  </head>

<body > 
<header>
      <div class="container" style="margin-top:10px;">
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-7">
            <a href="../home" rel="logo" class="">
              <img src="../images/logo.svg"  alt="Logo" class="img-responsive text-center logo">
            </a>
          </div>


              <nav class="navbar navbar-default wrap home">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul id="nav" class="nav navbar-nav">
                        <li >
                          <a class="home" href="#via-allegra" >HOME</a> 
                        </li>
                        <li><a class="nav-nosotros" href="#nosotros">ABOUT US</a></li>
                        <li><a class="nav-locales" href="#locales">SHOPS</a></li>
                        <li><a class="nav-eventos" href="#eventos" >EVENTS</a></li>
                        <li><a class="nav-galeria" href="#galeria" >GALLERY</a></li>
                        <li><a class="nav-contacto" href="#contacto" >CONTACT</a></li>
                        <li><a  href="../inicio" rel=""><img src="../images/es.png" class="img-responsive idioma-inactivo"></a></li>
                        <li><a href="../home/" rel=""><img src="../images/en.png" class="img-responsive idioma-activo"></a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
              </nav>

          
      </div>
</header>
    <video id="video" autoplay muted loop no-controls playsinline>
              <source src="../images/video.mp4" type='video/mp4'/>
                su navegador no soporta videos
    </video>
     <div class="controles">
      <i onclick="Mute()" id="silenciar" class="fas fa-volume-up"></i>
      <i onclick="enable()" id="ruido" class="fas fa-volume-mute"></i>
    </div>
<script>
      var vid = document.getElementById("video");

      function enable() { 
          vid.muted = false;
          $("#ruido").hide();
          $("#silenciar").show();
      }
      function Mute() { 
          vid.muted = true;
          $("#ruido").show();
          $("#silenciar").hide();
      }
</script>


    <section class="via-allegra" id="via-allegra">
      <div class="container">
        <div class="col-md-6">
          <h1>VIA ALLEGRA</h1>
          <p class="margin">Our inspiration for Paseo Via Allegra comes from European glamour and lifestyle. This space is home to prestigious national and international brands, where sophistication and good taste can be found in each and every corner.</p>
          <p class="margin">This boutique shopping mall offers a broad variety of high-quality products that are part of the latest world trends.</p>
        </div>
         <div class="col-md-6">
                <img src="../images/viaalegra.png">
          </div>
      </div>
    </section>
    <section class="nosotros" id="nosotros">
      <div class="container">
        <div class="col-md-6 hidden-xs">
          <img src="../images/nosotros.png">
        </div>
        <div class="col-md-5 texto-nosotros">
          <h1>ABOUT US</h1>
          <p class="margin">Its Italian architecture provides an elegant and relaxed atmosphere. Located on the trendiest street of the city—Senador Long—this boutique shopping mall invites its visitors to discover the latest trends in fashion, beauty and decoration; and this street contains dozens of high fashion clothing stores, restaurants and bars that are frequented by chic crowds of all ages, both during the day and at night.</p>
        </div>
        <div class="col-md-6 hidden-lg hidden-md">
          <img src="../images/nosotros.png">
        </div>
      </div>
    </section>
<section class="locales" id="locales">
  <div class="container">
            <h1>SHOPS</h1>
      <div class="row hidden-xs" style="margin-left:100px;">

          <div class="centerflipcards">
            <?php
            $sql = "SELECT * FROM tiendas ";
              $result = $db->query($sql);
               if ($result->num_rows > 0) {      
                while($row = $result->fetch_assoc()) {
                    echo'<div class="square-flip">
                          <div class="square" >
                            <div class="square-container">
                              <div class="align-center"></div>
                              <p>'.$row["name"].'</p>
                              <div class="linea"></div>
                            </div>
                            <div class="flip-overlay"></div>
                          </div>
                          <div class="square2">
                            <div class="square-container2">
                              <div class="align-center"></div>
                              <a href="../store/?id='.$row["id"].'">
                              <center> <img src="'.$url.''.$row["logo_dir"].''.$row["logo"].'"></center>
                              </a>
                            </div>
                            <div class="flip-overlay"></div>
                          </div>
                        </div>';
                   }}?>


              <div class="clearfix"></div>
          </div>  
      </div>
      <div class="row hidden-md hidden-lg" style="margin:50px;">
          <?php
            $sql = "SELECT * FROM tiendas ";
              $result = $db->query($sql);
               if ($result->num_rows > 0) {      
                while($row = $result->fetch_assoc()) {
                    echo'<div class=" local-mob">
                          <div class="recuadro-mob">
                            <a href="../store/?id='.$row["id"].'">
                              <center><img src="'.$url.''.$row["logo_dir"].''.$row["logo"].'"></center>
                             </a>
                          </div>
                        </div>';
                   }}?>
            
             
      </div>
  </div>
</section>
<section class="eventos" id="eventos">
  <div class="container ">
          <h1>EVENTS</h1>
          <br>
          <p>Since its opening in 2001, Via Allegra has been the stage for exclusive events and activities where protagonism is shared with brands, coolhunters and the public in general.</p>
          <p><i>Cocktails, concerts and brand launches</i></p>
          <p>Via Allegra provides the ideal environment for cocktails, concerts and brand launches. Thus, besides hosting launch parties for its shops new products and brands, Via Allegra has also hosted elegant fashion shows organized by renowned companies and individuals such as Paraguay Alta Moda, “Paseo Sedal”, Fabiana Casadio, and Roberto Pineiro, which have in turn been broadcasted by important broadcast media like E! Entertainment. In addition, Via Allegra has also surprised its clients throughout the years with prize draws for luxurious gifts from its exclusive shops, as well as trips to exotic destinations.</p>
          <p><i> Special dates</i></p>
          <p>Likewise, Via Allegra carries out different activities that change year by year on special dates such as Valentines Day, Mother’s Day, Father’s Day and Christmas, surprising its clients with direct benefits from their purchases.</p>

  <div class="row " >

          <?php
            $sql = "SELECT * FROM actividades order by orden ASC";
              $result = $db->query($sql);
               if ($result->num_rows > 0) {      
                while($row = $result->fetch_assoc()) {
                    echo'
                      <div class="col-md-4 col-12 evento" >
                        <a href="../event/?id='.$row["id"].'">
                        <p>'.$row["name"].'</p>
                        <img src="'.$url.''.$row["logo_dir"].''.$row["logo"].'">
                        </a>    
                      </div>';}}?>

    </div>
  </div>
</section>


<section class="galeria" id="galeria">
  <div class="container ">
          <h1>GALLERY</h1>

      <div class="galeria-p row">
      
        <div class=" col-md-7">

         <iframe id="video-ytb" style="display:none;" width="500" height="400" src="https://www.youtube.com/embed/" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           
        <ul id="lightgallery">

          <?php $sql = "SELECT * FROM galeria ";
            $con=1;
           $result = $db->query($sql);
            if ($result->num_rows > 0) {      
              while($row = $result->fetch_assoc()) {
                if ($con==1) {
                    if (!empty($row["foto"])) {
                      echo'<li id="img-principal" data-responsive="'.$url.''.$row["foto_dir"].''.$row["foto"].'" data-full="'.$url.''.$row["foto_dir"].''.$row["foto"].' " data-src="'.$url.''.$row["foto_dir"].''.$row["foto"].'" data-full="'.$url.''.$row["foto_dir"].''.$row["foto"].' ">
                                  <a href="">
                                    <img id="img-responsive" src="'.$url.''.$row["foto_dir"].''.$row["foto"].'" data-full="'.$url.''.$row["foto_dir"].''.$row["foto"].' ">
                                  </a>
                                </li>';
                     }  

                  $con=2;
                }else{
                  if (!empty($row["foto"])) {
                       echo' <li style="display:none;" data-responsive="'.$url.''.$row["foto_dir"].''.$row["foto"].'" data-full="'.$url.''.$row["foto_dir"].''.$row["foto"].' " data-src="'.$url.''.$row["foto_dir"].''.$row["foto"].'" data-full="'.$url.''.$row["foto_dir"].''.$row["foto"].' ">
                                <a href="">
                                  <img id="img-responsive" src="'.$url.''.$row["foto_dir"].''.$row["foto"].'" data-full="'.$url.''.$row["foto_dir"].''.$row["foto"].' ">
                                </a>
                              </li>
                            ';
                  }
                }
              }}?>
              
            </ul>
        </div>
      

          <!--Gallery Thumbs-->
          <div class="gallery__thumbs col-md-3">
            <?php $sql = "SELECT * FROM galeria ";
                  $result = $db->query($sql);
                   if ($result->num_rows > 0) {      
                    while($row = $result->fetch_assoc()) {
                      if (!empty($row["foto"])) {
                       echo'<img class="cambiar'.$row["id"].'" src="'.$url.''.$row["foto_dir"].''.$row["foto"].'">
                          <script>
                            $(".cambiar'.$row["id"].'").click(function(){
                                $("#img-principal").attr("data-responsive", "'.$url.''.$row["foto_dir"].''.$row["foto"].'");
                                $("#img-principal").attr("data-src", "'.$url.''.$row["foto_dir"].''.$row["foto"].'");
                                $("#img-principal").attr("data-full", "'.$url.''.$row["foto_dir"].''.$row["foto"].'");
                                $("#img-responsive").attr("src", "'.$url.''.$row["foto_dir"].''.$row["foto"].'");
                                $("#img-responsive").attr("data-full", "'.$url.''.$row["foto_dir"].''.$row["foto"].'");
                                $("#lightgallery").show();
                                $( "#video-ytb" ).attr("style", "display:none;");
                            });
                           </script>'
                          ;
                      }elseif (!empty($row["url"])) {
                        echo'<img class="cambiar'.$row["id"].'" src="https://img.youtube.com/vi/'.$row["url"].'/1.jpg"><i class="far fa-play-circle"></i>

                          <script>
                            $(".cambiar'.$row["id"].'").click(function(){
                                 $("#lightgallery").attr("style", "display:none;");
                                 $( "#video-ytb" ).show();
                                 $("#video-ytb").attr("src", "https://www.youtube.com/embed/'.$row["url"].'");
                            });
                           </script>'
                          ;
                      }
                      

                        }}?>
          

          </div>
          <!--Gallery Thumbs-->

      </div>
 </div>
</section>
 

<section class="contacto " id="contacto">
  <div class="container ">
     <h1>CONTACT</h1>
     <div class="row">
       <div class="col-md-5 col-12 margin formulario">
         <form>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label >Name</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label >Last Name</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label >Email</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label >Message</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
              </div>
            </div>
            <div class=" col-12 col-md-6">
               <div class="g-recaptcha" data-sitekey="6Lfn25QUAAAAAGwEsZfoduUauWsKNUS71-M8x6J1"></div>
            </div> 
            <div class="col-12 col-md-6">
              <center>
                <button type="button" class="boton">Send</button>
              </center>
            </div>
          </div>
         </form>
       </div>
       <div class="col-md-4 col-12 info">
        <h2>Paseo Via Allegra</h2>

        <p class="sub-title">Administration & Rentals</p>

        <p class="texto">For information on the organization of events(e.g. fashion shows) inside the paseo, please contact the administration on Paseo Vía Allegra at:
       <p class="ubi"><a style="color: black; text-decoration: none;" href="https://www.google.com/maps/dir//via+allegra/@-25.282045,-57.5798028,16z/data=!4m8!4m7!1m0!1m5!1m1!1s0x945da89f8dd657a7:0x88aec20f61ed2bf0!2m2!1d-57.576394!2d-25.28664" target="_blank"><span class="icono-ubi"><i class="fas fa-map-marker-alt"></i></span>&nbsp;&nbsp;Senador Long esq. Tte. Vera</a></p>

        <p class="cel"><a style="color: black; text-decoration: none;" href="tel:+59521600077"><span class="icono-mail"><i class="fas fa-phone"></i></span>&nbsp;&nbsp;+595 21 600-077</a></p>
         <p class="cel"><a style="color: black; text-decoration: none;" href="tel:+595971800444"><span class="icono-cel"><i class="fas fa-mobile-alt"></i></span>&nbsp;&nbsp;+595 971 800-444</a></p>
        <p class="mail"><a style="color: black; text-decoration: none;" href="mailto:info@viaallegra.com.py"><span class="icono-mail"><i class="far fa-envelope"></i></span> &nbsp;&nbsp;info@viaallegra.com.py</a></p>

         <p class="mail hidden-xs"><span class="icono-mail"><i class="fa fa-car"></i></span> &nbsp;&nbsp;
Valet parking and free parking for<br> &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;clients.</p>
        <p class="mail hidden-lg hidden-md"><span class="icono-mail"><i class="fa fa-car"></i></span> &nbsp;&nbsp;Valet parking and free parking<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for clients.</p>
        <br>
        <p class=" mail"><span class="icono-mail"><i class="far fa-clock"></i></span>&nbsp;&nbsp;<b> Hours </b><br>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<b>Juan Valdez Café:</b><br>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<small>Monday to Sunday: 7:00hs - 21:00hs</small><br>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<b>Other Shops:</b><br>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<small>Monday to Saturday: 8:30hs - 20:30hs</small><br>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<b>Office hours:</b><br>
          &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<small>Monday to Friday: 8:00hs - 16:00hs</small><br>
       </div>

      
     </div>
      <div class="col-md-10 col-12 mapa">
        <iframe src="../mapa.html" frameborder="0" width="100%" ></iframe>
      </div>

  </div>
</section>
<footer>
<center>
  <p>Senador Long esq. Tte. Vera | Asunción, Paraguay</p>
  <p><a style="color: black; text-decoration: none;"  href="tel:+59521600077">+ 595 21 600077</a ></p>
   <div class="redes row-centered">
      <a class="facebook" href="https://www.facebook.com/PaseoViaAllegra/"><i class="fab fa-facebook-f"></i></a>
      <a class="twitter" href="https://www.twitter.com/ViaAllegraPy"><i class="fab fa-twitter"></i></a>
      <a class="instagram" href="https://www.instagram/PaseoViaAllegra"><i class="fab fa-instagram"></i></a>
    </div>
</center>
</footer>
<div class="firma">
<center >developed by <a href=""><img style="width: 35px;" src="../images/linco.svg"></a> </center>
</div>

</body>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/index.js"></script>
<script src="../js/nav.js"></script>
    <script>
          $('.navbar-nav>li>a').on('click', function(){
          $('.navbar-collapse').collapse('hide');
      });
     
      $(document).ready(function() {
        $('#nav').onePageNav();
      });


      lightGallery(document.getElementById('lightgallery'))
    </script>
   
</html>